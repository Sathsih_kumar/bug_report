from django.conf.urls import patterns, include, url
import os.path
STATIC_ROOT=os.path.join(os.path.dirname(__file__),'templates/static')
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from bug.views import *
urlpatterns = patterns('',
    # Examples:

    url(r'^$',home),
    url(r'^session_login/$', session_login),
    url(r'^logout/$',logout),
    url(r'^index/$',index),
    url(r'^app_admin/$',app_admin),
    url(r'^issue/$',issue),
    url(r'^user_page/$',user_page),
    url(r'^pending/$',pending),
    url(r'^profile/$',profile),
    url(r'^validate/$',validate),
    url(r'^prof_update/$',prof_update),
    url(r'^forget_password/$',forget_password),
    url(r'^prof_cancel/$',prof_cancel),
    url(r'^send_email/$',send_email),
    url(r'^remark/(?P<issue_id>\d+)/$',remark),
    url(r'^remark_update/(?P<is_id>\d+)/$',remark_update),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': STATIC_ROOT}),
    # url(r'^bug_report/', include('bug_report.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
