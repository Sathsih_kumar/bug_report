from django.db import models
# Create your models here.
class login_details(models.Model):
	user_name=models.CharField(max_length=30)
	password = models.CharField(max_length=30)
	email=models.EmailField(max_length=70,blank=True, null= True, unique= True)
	login_type=models.PositiveSmallIntegerField(default=0, blank=True, null=True)
	def __str__(self):
		return '%s' % (self.user_name)
	
class issues_details(models.Model):
	user_id = models.ForeignKey(login_details)
	issue=models.CharField(max_length=250)
	status=models.CharField(max_length=30)
	created_on=models.DateTimeField()
	resolve_on=models.DateTimeField(blank=True, null=True)
	resolved_by=models.CharField(max_length=30)
	remark=models.CharField(max_length=450)
	note=models.CharField(max_length=450)
	def __str__(self):
		return '%s' % (self.issue)

class Admin:
	pass
	
