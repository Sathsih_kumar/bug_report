# Create your views here.
from django.shortcuts import *
from bug.models import *
from django.db.models import Q
import datetime
from django.core.mail import send_mail, BadHeaderError
#from django.core.context_processors import csrf
def home(request):
	if 'member_id' in request.session:
		if request.session['type']==1:
			return HttpResponseRedirect("/app_admin/")
		else:
			return HttpResponseRedirect("/index/")
	else:
		return render(request,'login.html')

def session_login(request):
	
		try:
			#import ipdb;ipdb.set_trace()
			m =login_details.objects.get(user_name=request.POST['username'])
			if m.password == request.POST['password'] :
				request.session['uname']=m.user_name
				request.session['member_id'] = m.id
				request.session['type']=m.login_type
				if m.login_type ==1:
					
					return HttpResponseRedirect("/app_admin")
				else:
					return HttpResponseRedirect("/user_page")
			else:
				return HttpResponseRedirect("/validate")

		except Exception:
				return HttpResponseRedirect("/validate")

def validate(request):
	wrong="**User name or Password is bad Please Check...**"
	#c = {}
	#c.update(csrf(request))
	return render_to_response("login.html",locals())

def logout(request):
	try:
		del request.session['member_id']
	except KeyError:
		pass
	return HttpResponseRedirect("/")
def index(request):
	return HttpResponseRedirect("/user_page")
def app_admin(request):
	user=request.session['uname']
	r=issues_details.objects.all()
	return render_to_response("admin.html",locals())

def issue(request):
	issue=request.GET['issue']
	p1=issues_details(user_id_id=request.session['member_id'],issue=issue,status='pending',created_on=datetime.datetime.now())
	p1.save()
	return HttpResponseRedirect("/")
def user_page(request):
	user=request.session['uname']
	q=issues_details.objects.filter(user_id=request.session['member_id'])
	return render_to_response("user.html",locals())
def pending(request):
	pid=request.GET.get('issue_id','')
	a=issues_details.objects.get(id=pid)
	a.login_details=None
	a.resolved_by=request.session['uname']
	a.status="Solved"
	a.resolve_on=datetime.datetime.now()
	a.save()
	return HttpResponseRedirect('/app_admin')
def profile(request):
	user=request.session['uname']
	return render_to_response("profile.html",locals())
def prof_update(request):
	current=request.POST['current']
	new=request.POST['new']
	conform=request.POST['conform']
	b=login_details.objects.get(user_name=request.session['uname'])
	c=b.password
	if c==current:
		if new==conform:
			b.password=new
			b.save()
			if b.login_type==1:
				return HttpResponseRedirect('/app_admin')
			else:
				return HttpResponseRedirect('/user_page')
	else:
		if b.login_type==1:
				return HttpResponseRedirect('/app_admin')
		else:
				return HttpResponseRedirect('/user_page')

def remark(request,issue_id):
	c=issues_details.objects.get(id=issue_id)
	user=request.session['uname']
	return render_to_response("remark.html",locals())
def prof_cancel(request):
	b=login_details.objects.get(user_name=request.session['uname'])
	if b.login_type==1:
		return HttpResponseRedirect('/app_admin')
	else:
		return HttpResponseRedirect('/user_page')
def remark_update(request,is_id):
	#import ipdb;ipdb.set_trace()
	pis=is_id
	y=issues_details.objects.get(id=pis)
	y.issue=request.GET['issue']
	y.status="Solved"
	y.remark=request.GET['remark']
	y.note=request.GET['note']
	y.resolved_by=request.session['uname']
	y.resolve_on=datetime.datetime.now()
	y.save()
	return HttpResponseRedirect('/app_admin')

def forget_password(request):
	return render_to_response('forget.html')

def send_email(request):
	try:
	    subject = "Password_change"
	    to_email = request.POST.get('email', '')
	    a=login_details.objects.get(email=to_email)
	    mess = "your password is "
	    message=mess+a.password
	    if a.email==to_email:
	    	if subject and message and to_email:
	    	    try:
	    	    	send_mail(subject, message,'sathish.kumar140@gmail.com', [to_email])
	    	    	a="Please Check your mail"
	    	    	return render_to_response("forget.html",locals())
	    	    except BadHeaderError:
	    	        return HttpResponse('Invalid header found.')
	    	else:
				return HttpResponseRedirect("/forget_password")
	except Exception:
		return HttpResponseRedirect("/forget_password")
